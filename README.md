## General requirements
1. Application code must be formatted in a consistent style and comply with the Java Code Convention.
2. If the application contains console menus or Input / Output, then they should be minimal, sufficient and intuitive. Language - English.
3. In the comments for the class, write your name and task number. Add meaningful comments to your code whenever possible.
4. Java Collection Framework (except java.util.Arrays) and regular expressions cannot be used

## Tasks options
Write a program for processing the array. The program should print the original array and the result to the console.
Implemented methods:
1. In an array of integers, swap the maximum negative element and the minimum positive
2. In an array of integers, determine the sum of the elements at even positions
3. In an array of integers, replace negative elements with zeros
4. In an array of integers, triple each positive element that comes before a negative
5. In an array of integers, find the difference between the arithmetic mean and the value of the minimum element
6. In an array of integers, output all the elements that occur more than once and whose indices are odd (if there are two identical numbers, but one has an even index, and the other does not, then output ** must **)

## Input / Output example
```
Original array:
-4	4	8	0	-5	1	8	5	2	7	5	5	6	0	7	-1	-7	-6	-2	9
1) Exchange max negative and min positive elements:
-4	4	8	0	-5	-1	8	5	2	7	5	5	6	0	7	1	-7	-6	-2	9
2) Sum of elements on even positions:
18
3) Replace negative values with 0
0	4	8	0	0	1	8	5	2	7	5	5	6	0	7	0	0	0	0	9
4) Multiply by 3 each positive element standing before negative one
-4	4	8	0	-5	1	8	5	2	7	5	5	6	0	21	-1	-7	-6	-2	9
5) Difference between average and min element in array:
9.1
6) Elements which present more than one time and stay on odd index
0	5	7
```